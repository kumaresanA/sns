
import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import {DepartmentsPage} from '../departments/departments';



@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController) {

  }

department(type:string){
  console.log(type)
  this.navCtrl.push(DepartmentsPage,type)
}
}
