import { CoursesPage } from './../courses/courses';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the DepartmentsPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-departments',
  templateUrl: 'departments.html',
})
export class DepartmentsPage {
  items;
  snstech=[
'AERO',
'AGRI',
'AUTO',
'BIO',
'CIVIL',
'CSE',
'C_PLAN',
'ECE',
'EEE',
'EIE',
'IT',
'MBA',
'MCA',
'MEA',
'MECH',
'MECHATRONICS',
'SCD'
  ];

  snseng=[
'CIVIL',
'CSE',
'ECE',
'EEE',
'IT',
'MBA',
'MCA',
'MECH',
'SCD'
  ];

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DepartmentsPage',this.navParams.data);
    if(this.navParams.data=='tech'){
      this.items=this.snstech
    }
    else{
      this.items=this.snseng;
    }
  }
 

courses(type:string){
  console.log(type)
  this.navCtrl.push(CoursesPage,type)
}

}
